import matplotlib.pyplot as pl
import numpy as np
import wradlib.vis as vis
import wradlib.clutter as clutter
import wradlib.util as util
import warnings
import xarray as xr
warnings.filterwarnings("ignore")
try:
    get_ipython().magic("matplotlib inline")
except:
    pl.ion()
import numpy as np
filename = util.get_wradlib_data_file("/home/sreelekshmi/Downloads/Aug22_076220/RCTLS_01JUN2021_061746_L2B_STD.nc")
data =xr.open_dataset(filename)
clmap = clutter.filter_gabella(data, wsize=5, thrsnorain=0.0, tr1=6.0, n_p=8, tr2=1.3)
fig = pl.figure(figsize=(12, 8))
ax = fig.add_subplot(121)
ax, pm = vis.plot_ppi(data, ax=ax)
ax.set_title("Reflectivity")
ax = fig.add_subplot(122)
ax, pm = vis.plot_ppi(clmap, ax=ax)
ax.set_title("Cluttermap")
plt.show()
