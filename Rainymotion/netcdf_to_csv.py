#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 12:59:25 2022

@author: binoy
"""
import xarray as xr
import os

# Replace 'local_storage_directory', 'netcdf_dir' and 'csv_dir' by respectively
# the directory path to Copernicus Marine data, the directory path to netcdf files
# and the directory path to csv files
local_storage_directory = '/home/binoy/rainymotion/rainymotion-master/data/'
netcdf_dir = local_storage_directory
csv_dir = local_storage_directory

# Replace the file name in quote by the file name of the netcdf file (.nc) you want to convert to csv
netcdf_file_name = 'data.nc'

# Set variables names for the input file.nc (netcdf_file_in) and the output file.csv (`csv_file_out`)
netcdf_file_in = netcdf_dir + netcdf_file_name
csv_file_out = csv_dir + netcdf_file_name[:-3] + '.csv'

ds = xr.open_dataset(netcdf_file_in)
df = ds.to_dataframe()

df.to_csv(csv_file_out)

