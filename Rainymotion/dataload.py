from rainymotion import models, metrics, utils
from collections import OrderedDict
import numpy as np
import h5py
import matplotlib.pyplot as plt
import xarray as xr
# import the model from the rainymotion library
from rainymotion.models import Dense

# initialize the model
model = Dense()

# upload data to the model instance
model.input_data = np.load("/home/sreelekshmi/Sreelekshmi/ICFOSS/data/test.npy",allow_pickle=True)

#ds = xr.open_dataset("/home/binoy/rainymotion/rainymotion-master/data/data.nc")

# run the model with default parameters
nowcast = model.run()
